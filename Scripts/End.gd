extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var scene  = preload("res://Scenes/Victory.tscn")
var timer
# Called when the node enters the scene tree for the first time.
func _ready():
	timer = get_parent().get_node("CanvasLayer/GUI/Timer")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_End_Area_body_entered(body):
	if body.get_name() == "Player":
		Global.play = false
		Global.finished = true
		timer.stop()
		get_parent().get_node("CanvasLayer").add_child(scene.instance())
		
