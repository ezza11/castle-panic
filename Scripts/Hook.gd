extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var ray = $RayCast2D
# Called when the node enters the scene tree for the first time.
func _ready():
	_process(true) # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mpos = get_global_mouse_position()
	look_at(mpos)
