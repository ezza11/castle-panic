extends KinematicBody2D

class_name Player

onready var hook := $Hook
onready var ray := $Hook/RayCast2D
onready var tween := $Hook/Tween

const GRAVITY_VEC = Vector2(0, 800)
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
const WALK_SPEED = 200 # pixels/sec
const DASH_SPEED = 1500
const JUMP_SPEED = 350
const DASH_MULTIPLIER = 3
const SIDING_CHANGE_SPEED = 0
const BULLET_VELOCITY = 1000
const SHOOT_TIME_SHOW_WEAPON = 0.2


var dash_frames = 0
var is_dashing = false
var anim = ""
onready var sprite = $Sprite
var linear_vel = Vector2()
var state = ""

func _physics_process(delta):
	get_parent().get_node("CanvasLayer/GUI/VBoxContainer/Label2").hide()
	minus_frames()
	var snap = Vector2(0, 10)
	## MOVEMENT ##
	# Applying gravity
	linear_vel += delta * GRAVITY_VEC
	# Move and slide
	linear_vel = move_and_slide(linear_vel, FLOOR_NORMAL)
	# Detect if we are on floor - only works if called *after* move_and_slide
	var on_floor = is_on_floor()
	if ray.is_colliding():
		get_parent().get_node("CanvasLayer/GUI/VBoxContainer/Label2").show()
	### CONTROL ###
	# Horizontal movement
	var target_speed = 0
	if on_floor and Input.is_action_just_pressed("jump"):
		snap = Vector2()
		$Jump_Sound.play()
		jump()
	if Input.is_action_just_released("jump")  and Global.finished == false:
		jump_cut()
	if Input.is_action_just_pressed("dash")  and Global.finished == false:
		$Dash_Sound.play()
		dash()
	if Input.is_action_just_pressed("action") and ray.is_colliding()  and Global.finished == false:
		$Hit_Sound.play()
		linear_vel = move_and_slide((ray.get_collision_point() - position).normalized() * 800)
	if Input.is_action_pressed("left") and Global.finished == false:
		state = "walk"
		target_speed -= 1
	if Input.is_action_pressed("right") and Global.finished == false:
		state = "walk"
		target_speed += 1
		
	# If game is finished
	if Input.is_action_just_pressed("jump")  and Global.finished == true:
		pass
	if Input.is_action_just_pressed("dash")  and Global.finished == true:
		pass
	if Input.is_action_just_pressed("action") and ray.is_colliding()  and Global.finished == true:
		pass
	if Input.is_action_pressed("left") and Global.finished == true:
		pass
	if Input.is_action_pressed("right") and Global.finished == true:
		pass
		
	
	if is_dashing:
		target_speed *= DASH_SPEED
		linear_vel.x = lerp(linear_vel.x, target_speed, 0.1)
	elif state == "walk":
		target_speed *= WALK_SPEED
		linear_vel.x = lerp(linear_vel.x, target_speed, 0.1)

	# Animation
	var new_anim = "Idle"
	if on_floor:
		if linear_vel.x < -10:
			sprite.scale.x = -2
			new_anim = "Run"
		if linear_vel.x > 10:
			sprite.scale.x = 2
			new_anim = "Run"
	else:
		new_anim = "Jump"

	if new_anim != anim:
		anim = new_anim
		($Anim as AnimationPlayer).play(anim)

func jump():
	linear_vel.y -= JUMP_SPEED

func jump_cut():
	if linear_vel.y < 0:
		linear_vel.y = -50

func dash():
	state = "dash"
	is_dashing = true
	dash_frames = 100

func minus_frames():
	if dash_frames >  0:
		dash_frames -= 25
	else:
		is_dashing = false
		dash_frames= 0
		
	
