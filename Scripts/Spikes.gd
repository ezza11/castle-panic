extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# Signal for all spikes in the level
func _on_Spikes_body_entered(body):
	if body.get_name() == "Player":
		Global.timer = 120
		Global.dead += 1
		Global.play = false
		get_tree().change_scene(str("res://Scenes/" + "GameOver" + ".tscn"))



func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		Global.timer = 120
		Global.dead += 1
		Global.play = false
		get_tree().change_scene(str("res://Scenes/" + "GameOver" + ".tscn"))
