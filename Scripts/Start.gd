extends Node

# Declare member variables here. Examples:
# var a = 2
var level
# Called when the node enters the scene tree for the first time.
var timer
var start
var countdown = 50
var go = false
func _ready():

	timer = get_parent().get_node("CanvasLayer/GUI/Timer")
	start = get_parent().get_node("CanvasLayer/GUI/VBoxContainer/StartLabel")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.play == true:
		countdown -= 1
	if countdown == 0:
		start.hide()


func _on_Start_Area_body_entered(body):
	if body.get_name() == "Player":
		get_parent().get_node("Start_Sound").play()
		countdown -=1
		timer.start()
		start.show()
		Global.play =  true
		
		

		
		
