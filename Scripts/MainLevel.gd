extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Level_Sound.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.timer == 0:
		get_tree().change_scene(str("res://Scenes/" + "GameOver" + ".tscn"))
