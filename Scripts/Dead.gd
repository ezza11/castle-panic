extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var scene_to_load
export(String) var scene_to_load2
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _process(delta):
	if Input.is_action_just_pressed("restart"):
		get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton2_pressed():
	Global.timer = 120
	get_tree().change_scene(str("res://Scenes/" + "MainMenu" + ".tscn"))
