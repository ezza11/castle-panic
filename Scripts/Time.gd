extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _process(delta):
	self.text = "Time Left : " + str(Global.timer) + " Seconds"


func _on_Timer_timeout():
	Global.timer -= 1 # Replace with function body.
