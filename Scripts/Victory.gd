extends ColorRect

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var a 
var b
# Called when the node enters the scene tree for the first time.
func _ready():
	a = 100
	b = 150 # Replace with function body.
	$Victory.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	a -= 1
	b -= 1
	if a == 0:
		$Label2.show()
		$Label3.show()
		$Sound.play()
	if b == 0:
		$Label4.show()
		$Label5.show()
		$Sound.play()
		
